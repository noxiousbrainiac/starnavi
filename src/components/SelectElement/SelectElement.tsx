import React, {ChangeEvent, FC} from 'react';
import {ModeI} from "../../types";
import './SelectElement.css';

interface Props {
    modes?: ModeI;
    onChange: (e: ChangeEvent<HTMLSelectElement>) => void
}

const SelectElement:FC<Props> = ({onChange, modes}: Props) => {
    return (
        <select
            className="selectElem"
            onChange={onChange}
        >
            <option value={modes?.easyMode?.field}>Easy mode</option>
            <option value={modes?.normalMode?.field}>Normal mode</option>
            <option value={modes?.hardMode?.field}>Hard mode</option>
        </select>
    );
};

export default SelectElement;