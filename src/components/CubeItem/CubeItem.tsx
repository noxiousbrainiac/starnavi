import React, {FC} from 'react';
import {ItemI} from "../../types";
import './CubeItem.css';

interface Props {
    item: ItemI;
    index: number;
    disabled: boolean;
    onChange: (index: number, cube: object) => void
}

const CubeItem: FC<Props> = ({onChange, item, index, disabled}: Props) => {
    return (
        <div
            key={index}
            className={`cube ${item.active && "active"} ${disabled && "disabled"}`}
            onMouseEnter={() => onChange(index, item)}
        >

        </div>
    );
};

export default CubeItem;