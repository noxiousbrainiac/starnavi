import React, {FC} from 'react';
import './ButtonElement.css';

interface Props {
    onClick: () => void;
    children: any;
    disabled: boolean;
}

const ButtonElement: FC<Props> = ({onClick, children, disabled}: Props) => {
    return (
        <button
            className="btn"
            onClick={onClick}
            disabled={disabled}
        >
            {children}
        </button>
    );
};

export default ButtonElement;