import React, {FC} from 'react';
import {ItemI} from "../../types";
import './CardItem.css'

const CardItem: FC<ItemI> = ({col, row}: ItemI) => {
    return (
        <div className="card">
            <span>row {row}</span>
            <span>col {col}</span>
        </div>
    );
};

export default CardItem;