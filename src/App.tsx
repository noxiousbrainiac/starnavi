import './App.css';
import React, {ChangeEvent, useCallback, useEffect, useState} from "react";
import {createSquare} from "./utils";
import {ItemI, ModeI} from "./types";
import CubeItem from "./components/CubeItem/CubeItem";
import SelectElement from "./components/SelectElement/SelectElement";
import CardItem from "./components/CardItem/CardItem";
import ButtonElement from "./components/ButtonElement/ButtonElement";

const App = () => {
    const [mode, setMode] = useState<ModeI>();
    const [selected, setSelected] = useState(5);
    const [cubes, setCubes] = useState<ItemI[]>([]);
    const [start, setStart] = useState(false);

    const fetchData = useCallback(async () => {
        try {
            const response = await fetch("https://demo1030918.mockable.io");
            const data = await response.json();

            setMode(data);
        } catch (e) {
            if (e) {
                throw new Error();
            }
        }
    }, []);

    const handleChangeCube = ( i: number, item: ItemI ) => {
        const copy = [...cubes];
        copy[i].active = !item.active;
        setCubes(copy);
    };

    const handleChangeMode = ( e: ChangeEvent<HTMLSelectElement> ) => {
        setSelected(+e.target.value);
        setStart(false);
    };

    useEffect(() => {
        const newCubes =  createSquare(selected);
        setCubes(newCubes);
    }, [selected]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    return (
        <div className="container">
            <div className="app">

                <div className={`square mode${selected}`}>
                    {cubes.map((cube, i) => (
                        <CubeItem
                            key={i}
                            item={cube}
                            index={i}
                            disabled={!start}
                            onChange={() => start && handleChangeCube(i, cube)}
                        />
                    ))}
                </div>

                <div>
                    <div className="appActions">
                        <SelectElement
                            modes={mode}
                            onChange={handleChangeMode}
                        />

                        <ButtonElement
                            onClick={() => setStart(true)}
                            disabled={start}
                        >
                            start
                        </ButtonElement>

                        <ButtonElement
                            onClick={() => setStart(false)}
                            disabled={!start}
                        >
                            stop
                        </ButtonElement>
                    </div>

                    <div>
                        {cubes.filter(item => item.active).map((elem, i) => (
                            <CardItem key={i} col={elem.col} row={elem.row}/>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;