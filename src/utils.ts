export const createSquare = (modeValue: number) => {
    const array = [];

    for (let i = 1; i <= modeValue; i++) {
        for (let y = 1; y <= modeValue; y++) {
            array.push({col: y, row: i, active: false});
        }
    }
    return array;
}