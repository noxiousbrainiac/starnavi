export interface ItemI {
    col?: number;
    row?: number;
    active?: boolean;
}

interface Field {
    field: number
}

export interface ModeI {
    easyMode: Field
    normalMode: Field
    hardMode: Field
}
